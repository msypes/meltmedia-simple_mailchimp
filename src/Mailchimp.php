<?php
/**
 * @file MailchimpSubscriber.php
 * @author Michael A. Sypes <michael@sypes.org>
 * @project d8_testbed
 *
 * @abstract
 * Service class for communicating with Mailchimp
 */

namespace Drupal\simple_mailchimp;

use \GuzzleHttp\Client;
use \Drupal\Core\Config\ConfigFactoryInterface;

class Mailchimp {

	/** @var \GuzzleHttp\Client $client */
	private $client;

	/** @var \Drupal\Core\Config\ImmutableConfig $api_config */
	private $api_config;

	/**
	 * MailchimpSubscriber constructor.
	 *
	 * @param \GuzzleHttp\Client $client
	 * @param \Drupal\Core\Config\ConfigFactoryInterface $configuration_factory
	 */
	public function __construct(Client $client, ConfigFactoryInterface $configuration_factory) {
		$this->client = $client;
		$this->api_config = $configuration_factory->get('simple_mailchimp.mailchimp');
	}

	/**
	 * @param string $email
	 *
	 * @return array
	 *  - message type string
	 *  - message string
	 * @throws \Exception
	 */
	public function subscribeEmail($email) {

		// Load mailchimp credentials via configuration system.
		$mailchimp_api_key = $this->api_config->get('api_key');
		$mailchimp_list_id = $this->api_config->get('list_id');
		$mailchimp_data_center = $this->api_config->get('data_center');

		$mailchimp_base_url = 'https://' . $mailchimp_data_center . '.api.mailchimp.com/3.0/';
		$subscriber_email = strtolower(trim($email));
		$mailchimp_subscribe_url = $mailchimp_base_url . 'lists/' . $mailchimp_list_id . '/members';

		// Load mailchimp form-submission messages
		$on_success = $this->api_config->get('success_msg');
		$on_already_subscribed = $this->api_config->get('already_subscribed_msg');
		$on_system_failure = $this->api_config->get('system_failure_msg');

		try {
			$response = $this->client->request('POST', $mailchimp_subscribe_url, [
				'auth' => ['apikey', $mailchimp_api_key],
				'json' => [
					'email_address' => $subscriber_email,
					'status' => 'subscribed',
				]
			]);
		}
		catch(\Exception $e) {
		    if ($e->getCode() == '400') {
				return ['warning', $on_already_subscribed];
			}
			else{
		    	throw $e;
			}
		}

		if (isset($response) && $response->getStatusCode() == '200') {
			return ['status', $on_success];
		}
		else {
			return ['error', $on_system_failure];
		}

	}

}