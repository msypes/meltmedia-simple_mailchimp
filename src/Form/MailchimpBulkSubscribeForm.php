<?php

/**
 * @file
 * Contains \Drupal\simple_mailchimp\Form\MailchimpBulkSubscribeForm.
 */

namespace Drupal\simple_mailchimp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MailchimpBulkSubscribeForm.
 *
 * @package Drupal\simple_mailchimp\Form
 */
class MailchimpBulkSubscribeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_bulk_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['emails'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Emails'),
      '#default_value' => '',
      '#description' => $this->t('Enter one email per line.')
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Subscribe all emails')
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  	/** @var \Drupal\simple_mailchimp\Mailchimp $client */
  	$client = \Drupal::service('simple_mailchimp.mailchimp');

  	$emails = explode("\n", $form['emails']['#value']);

  	foreach($emails as $email){
	    /** @var string $subscriber_email */
	    $subscriber_email = strtolower(trim($email));

	    // Check for proper email structure, since this only a text field.
	    // Pattern from http://emailregex.com/, matches HTML email input type
		if(preg_match('/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/', $subscriber_email)){
			list($message_type, $message) = $client->subscribeEmail($subscriber_email);

			drupal_set_message($this->t($subscriber_email.': @message', array('@message' => $message)), $message_type);
		}

    }

  }

}
