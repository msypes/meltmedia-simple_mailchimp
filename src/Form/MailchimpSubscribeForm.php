<?php

/**
 * @file
 * Contains \Drupal\simple_mailchimp\Form\MailchimpSubscribeForm.
 */

namespace Drupal\simple_mailchimp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MailchimpSubscribeForm.
 *
 * @package Drupal\simple_mailchimp\Form
 */
class MailchimpSubscribeForm extends FormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['email'] = array(
      '#type' => 'email',
      '#size' => '22',
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('simple-mailchimp--email-field')
      )
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#attributes' => array(
        'class' => array('simple-mailchimp--submit-button')
      )
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  	/** @var string $subscriber_email */
  	$subscriber_email = strtolower(trim($form['email']['#value']));

  	/** @var \Drupal\simple_mailchimp\Mailchimp $client */
	$client = \Drupal::service('simple_mailchimp.mailchimp');

	list($message_type, $message) = $client->subscribeEmail($subscriber_email);

    if ($message_type === 'error') {
	    $form_state->setRedirect('contact.site_page');
    }

    drupal_set_message($this->t('@message', array('@message' => $message)), $message_type);

  }
}
