# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

You will find a zip file attached to this email with a Drupal 8 module that is in need of refactoring and improvement. Your challenge is to refactor the module and provide unit testing with the fixed behavior by End of Day, November 13. (End of day is really before 9am the next day.) You're welcome to submit it earlier if you wish.

The module is called Simple Mailchimp, and it was written in-house. The main purpose of this module is to provide a highly configurable email subscription block. This block contains a form with an email field. When the form is processed, Drupal will attempt to subscribe the email address by making an HTTP request to a mailchimp endpoint passing the email address, mailchimp api key, and mailing list id. The code to make this HTTP request is currently part of the submitForm method in the MailchimpSubscribeForm class. This module also provides an interface to subscribe multiple emails (bulk subscription), but the implementation is incomplete. Additionally, this module has two administrative pages: Simple Mailchimp Settings and the Bulk Subscription. Access to these pages is limited to users with roles that have the 'access administration pages' permission.

Your challenge is to refactor the module and provide unit testing with the fixed behavior.

Here are the expected items that should be completed:

* Create a service class (Mailchimp) and add a method (subscribeEmail) that takes an email as a parameter. This method will connect with mailchimp to subscribe an individual email address.
* Write unit tests to ensure that subscribeEmail works as expected.
* Refactor the submitForm method in MailchimpSubscribeForm by calling subscribeEmail.
* Complete the implementation of the submitForm method on MailchimpBulkSubscribeForm by parsing the list of emails and calling subscribeEmail.
* Define a new permission to allow access to Simple Mailchimp Settings and another permission that allows access to Bulk Subscription.

To validate the integration of the code with the Mailachimp API you can choose one of two approaches:

* Leverage testing Mocks to mimic the behavior of the Mailchimp API while running the automated tests, based on the behavior from Mailchimps API documentation. This is the recommended approach.
* Integrate directly with Mailchimp. This will require setting up a mailchimp account in order to attain the needed API key.

Here are things we will also be looking for from the submission:

* Code quality.
* Object Oriented PHP.
* Knowledge of Drupal permissions.
* Knowledge of Drupal 8 service classes.
* Automated testing.


### How do I get set up? ###

Install like any other custom module for Drupal version 8