<?php
/**
 * @file MailchimpTest.php
 * @author Michael A. Sypes <michael@sypes.org>
 * @project d8_testbed
 *
 * @abstract
 */

namespace Drupal\Tests\simple_mailchimp;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\simple_mailchimp\Mailchimp;
use \Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;

/**
 * Class MailchimpTest
 * @package Drupal\Tests\simple_mailchimp\Unit
 * @group simple_mailchimp
 */
class MailchimpTest extends UnitTestCase {
	public function testSubscribeEmailGivesSuccessMessageWhenSuccessfullySubscribed(){

		// Mock Response.
		$response = $this->getMockResponse('200');

		// Mock the Guzzle client.
		$client = $this->getMockClient( $response );

		// Mock Configuration
		$config = $this->getMockConfiguration();

		// Mock Config Factory
		$config_factory = $this->getMockConfigFactory( $config );


		$service = new Mailchimp($client, $config_factory);

		$response = $service->subscribeEmail('some@email');

		$this->assertTrue(is_array($response));
		$this->assertEquals('status', $response[0]);
		$this->assertEquals('success message', $response[1]);
	}

	public function testSubscribeEmailGivesErrorMessageWhenNoResponse(){

		// Mock the Guzzle client.
		$client = $this->getMockClient( null );

		// Mock Configuration
		$config = $this->getMockConfiguration();

		// Mock Config Factory
		$config_factory = $this->getMockConfigFactory( $config );


		$service = new Mailchimp($client, $config_factory);

		$response = $service->subscribeEmail('some@email');

		$this->assertTrue(is_array($response));
		$this->assertEquals('error', $response[0]);
		$this->assertEquals('system failure message', $response[1]);
	}

	public function testSubscribeEmailGivesErrorMessageWhenResponseStrange(){

		// Mock Response.
		$response = $this->getMockResponse('not 200');

		// Mock the Guzzle client.
		$client = $this->getMockClient( $response );

		// Mock Configuration
		$config = $this->getMockConfiguration();

		// Mock Config Factory
		$config_factory = $this->getMockConfigFactory( $config );


		$service = new Mailchimp($client, $config_factory);

		$response = $service->subscribeEmail('some@email');

		$this->assertTrue(is_array($response));
		$this->assertEquals('error', $response[0]);
		$this->assertEquals('system failure message', $response[1]);
	}

	public function testSubscribeEmailGivesWarningMessageWhenAlreadySubscribed(){

		// Mock Exception Response.
		$response = new \Exception('mailchimp said this is already subscribed', 400);


		// Mock the Guzzle client.
		$client_prophecy = $this->prophesize(Client::class);
		$client_prophecy->request('POST', Argument::any(), Argument::any(), Argument::any())->shouldBeCalled()->willThrow($response);
		$client = $client_prophecy->reveal();

		// Mock Configuration
		$config = $this->getMockConfiguration();

		// Mock Config Factory
		$config_factory = $this->getMockConfigFactory( $config );


		$service = new Mailchimp($client, $config_factory);

		$response = $service->subscribeEmail('some@email');

		$this->assertTrue(is_array($response));
		$this->assertEquals('warning', $response[0]);
		$this->assertEquals('already subscribed message', $response[1]);
	}

	/**
	 * @return object
	 */
	private function getMockConfiguration() {
		$config_prophecy = $this->prophesize( ImmutableConfig::class );
		$config_prophecy->get( 'api_key' )->shouldBeCalled();
		$config_prophecy->get( 'list_id' )->shouldBeCalled();
		$config_prophecy->get( 'data_center' )->shouldBeCalled();
		$config_prophecy->get( 'success_msg' )
		                ->shouldBeCalled()
		                ->willReturn( 'success message' );
		$config_prophecy->get( 'already_subscribed_msg' )
		                ->shouldBeCalled()
		                ->willReturn( 'already subscribed message' );
		$config_prophecy->get( 'system_failure_msg' )
		                ->shouldBeCalled()
		                ->willReturn( 'system failure message' );

		return $config_prophecy->reveal();
	}

	/**
	 * @param $config
	 *
	 * @return object
	 */
	private function getMockConfigFactory( $config ) {
		$config_factory_prophecy = $this->prophesize( ConfigFactoryInterface::class );
		$config_factory_prophecy->get( 'simple_mailchimp.mailchimp' )
		                        ->shouldBeCalled()
		                        ->willReturn( $config );

		return $config_factory_prophecy->reveal();
	}

	/**
	 * @return object
	 */
	private function getMockResponse($response_code) {
		$response_prophecy = $this->prophesize( ResponseInterface::class );
		$response_prophecy->getStatusCode()
		                  ->shouldBeCalled()
		                  ->willReturn( $response_code );

		return $response_prophecy->reveal();
	}

	/**
	 * @param $response
	 *
	 * @return object
	 */
	private function getMockClient( $response ) {
		$client_prophecy = $this->prophesize( Client::class );
		$client_prophecy->request( 'POST', Argument::any(), Argument::any(), Argument::any() )
		                ->shouldBeCalled()
		                ->willReturn( $response );

		return $client_prophecy->reveal();
	}

}